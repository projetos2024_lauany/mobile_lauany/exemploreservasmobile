import React, { useState } from "react";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { View, Button } from "react-native";

const DateTimePicket = ({ type, buttonTitle, dateKey, setSchedule }) => { //funciona para pegar data e hora. dentro das chaves, estamos recebendo alguns parâmetros da página home
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);  //const para conseguir alterar o estado do component de data para quando clicar no modal, ele abrir um relógio nativo do celular
    const showDatePicker = () => { // com a função showDatePicker, criamos um datePicker para abrir e outro para fechar
        setDatePickerVisibility(true);  // abre calendário
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);  // fecha calendário
    };

    const handleConfirm = (date) => {
        if (type === "time") {
            //lógica para extrair hora e minuto, se não for, automaticamente é uma data e pra isso utilizamos o else 
            const hour = date.getHours();
            const minute = date.getMinutes();

            // Formatar hora e minuto como desejado
            const formattedTime = `${hour}:${minute}`;

            //Atualiza o estado da reserva com HH:MM formatado
            setSchedule((prevState) => ({
                ...prevState,
                [dateKey]: formattedTime,
            }));
        } else {
            //lógica para atualizar o schedule, manter o estado anterior do schedule antes de fazer uma atualização por meio de HH:mm
             const formattedDate = date.toISOString().split('T')[0];
            setSchedule((prevState) => ({
                ...prevState,
                [dateKey]: formattedDate,
            }));
        };
        hideDatePicker();
    };
    //contrução do component
    return (
        <View>
            <Button title={buttonTitle} onPress={showDatePicker} color="black" /> 
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode={type}
                locale="pt_BR"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}

                //estilização opcional para IPhone
                pickerContainerStyleIOS={{ backgroundColor: "#fff" }}
                textColor="#000" // Cor do texto
            />
        </View>
    );
};
export default DateTimePicket;
//Um datePicket, permite que os usuários escolham uma data específica a partir de um calendário visual.
